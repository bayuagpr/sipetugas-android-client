package android.google.com.androidspeedometer;

public class ApiUtils {
    private ApiUtils() {}

    public static final String BASE_URL = "https://sipetugas.web.id/";

    public static APIService getAPIService() {

        return RetrofitClient.getClient(BASE_URL).create(APIService.class);
    }
}
