package android.google.com.androidspeedometer;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrefActivity extends AppCompatActivity
{
  private SeekBar seekBar;
  private TextView textViewSetSpeed, textViewBus, textViewSupir;
  private Spinner spinnerViewBus, spinnerViewSupir;
  private RadioGroup radioGroup;
  private ToggleButton toggle;
  private String myPref_type, onOrOff, user_top_speed;
  private Button btnSave;
  private SharedPreferences pref;
  private String typeSatuan;
  private APIService mAPIService;
  private List<Bus> mBusList;
  private List<Supir> mSupirList;
  private Context mContext;
  private Integer namaPosition,platPosition;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    mContext = this;
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_pref);

    initialiseWidgets();

    this.pref = getApplicationContext().getSharedPreferences("speedPref", MODE_PRIVATE);

    //Reading from SharedPreferences - gets which preference type
    this.myPref_type = pref.getString("pref_type", null);

    mAPIService = ApiUtils.getAPIService();

    getListBus();

    getListSupir();

    spinnerViewBus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selectedPlat = parent.getItemAtPosition(position).toString();
        String[] parts = selectedPlat.split("\\.");

          String aIdBus = parts[0]; // 004
          String aPlatNomor = parts[1]; // 034556
          Log.d("id bus",""+aIdBus);
          Log.d("plat nomor", ""+aPlatNomor);
          platPosition = Integer.parseInt(aIdBus);
          Log.d("setelah parse",""+platPosition);
          Toast.makeText(mContext, "Bus dengan plat nomor " + aPlatNomor, Toast.LENGTH_SHORT).show();



      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });

    spinnerViewSupir.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selectedNama = parent.getItemAtPosition(position).toString();
        String[] parts = selectedNama.split("\\.");

          String aIdSupir = parts[0]; // 004
          Log.d("id_supir",""+aIdSupir);
          String aNamaSupir = parts[1]; // 034556
          namaPosition = Integer.parseInt(aIdSupir);
          Toast.makeText(mContext, "Supir dengan nama " + aNamaSupir, Toast.LENGTH_SHORT).show();



      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });

    if (this.myPref_type != null)
    {
      setRadioButtonChecked(this.myPref_type);
    }

    this.textViewSetSpeed.setText("0 " + myPref_type);

    this.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
    {

      @Override
      public void onCheckedChanged(RadioGroup group, int checkedId)
      {
        // find which radio button is selected
        if (checkedId == R.id.radioButton1)
        {
          typeSatuan="ms";
          //saveTypeToPrefs("ms");
        } else if (checkedId == R.id.radioButton2)
        {
          typeSatuan="mph";
          //saveTypeToPrefs("mph");
        } else if (checkedId == R.id.radioButton3)
        {
          typeSatuan="kph";
          //saveTypeToPrefs("kph");
        }
      }

    });

    //
    this.seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
    {
      int progress = 0;
      String mProgress;

      @Override
      public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser)
      {
        //progress = progresValue;
        this.mProgress = String.valueOf(progresValue);
        textViewSetSpeed.setText(this.mProgress + " " + myPref_type);
      }

      @Override
      public void onStartTrackingTouch(SeekBar seekBar)
      {
      }

      @Override
      public void onStopTrackingTouch(SeekBar seekBar)
      {
        textViewSetSpeed.setText(this.mProgress + " " + myPref_type);
      }
    });

    toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
    {
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
      {
        if (isChecked)
        {
          toggle.setBackgroundResource(R.color.colorGreen);
        } else
        {
          toggle.setBackgroundResource(R.color.colorRed);
        }
      }
    });

    this.btnSave.setOnClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(View v)
      {
        user_top_speed = textViewSetSpeed.getText().toString();
        onOrOff = toggle.getText().toString();
        String[] splited = user_top_speed.split("\\s+");
        String speed1 = splited[0];
        saveTypeToPrefs(typeSatuan);
        saveSpeedToPrefs(speed1, onOrOff);
        saveSupirToPrefs(namaPosition);
        saveBusToPrefs(platPosition);
        openDialog();

      }
    });
  }


  // Writing data to SharedPreferences
  private void saveTypeToPrefs(String mValue)
  {
    SharedPreferences.Editor editor = pref.edit();
    editor.remove("pref_type");
    editor.apply(); // commit changes
    editor.putString("pref_type", mValue);

    // Save the changes in SharedPreferences
    editor.apply(); // commit changes
  }

  // Writing data supir to SharedPreferences
  private void saveSupirToPrefs(Integer namaSupirInt)
  {
    SharedPreferences.Editor editor = pref.edit();
    editor.remove("nama_supir_int");
    editor.apply(); // commit changes
    editor.putString("nama_supir_int", String.valueOf(namaSupirInt));

    // Save the changes in SharedPreferences
    editor.apply(); // commit changes
  }

  // Writing data supir to SharedPreferences
  private void saveBusToPrefs(Integer platBusInt)
  {
    SharedPreferences.Editor editor = pref.edit();
    editor.remove("plat_bus_int");
    editor.apply(); // commit changes
    editor.putString("plat_bus_int", String.valueOf(platBusInt));

    // Save the changes in SharedPreferences
    editor.apply(); // commit changes
  }

  // Writing data to SharedPreferences
  private void saveSpeedToPrefs(String val1, String val2)
  {
    SharedPreferences.Editor editor = pref.edit();
    editor.remove("top_speed");
    editor.remove("on_off");
    editor.apply(); // commit changes
    editor.putString("top_speed", val1);
    editor.putString("on_off", val2);

    // Save the changes in SharedPreferences
    editor.apply(); // commit changes
  }


  private void setRadioButtonChecked(String mValue)
  {
    switch (mValue)
    {
      case "ms":
        RadioButton rad1 = (RadioButton) findViewById(R.id.radioButton1);
        rad1.setChecked(true);
        break;

      case "mph":
        RadioButton rad2 = (RadioButton) findViewById(R.id.radioButton2);
        rad2.setChecked(true);
        break;

      default:
        RadioButton rad3 = (RadioButton) findViewById(R.id.radioButton3);
        rad3.setChecked(true);
        break;
    }
  }

  private void initialiseWidgets()
  {
    this.textViewSetSpeed = (TextView) findViewById(R.id.textViewSetSpeed);
    this.seekBar = (SeekBar) findViewById(R.id.seekBar1);
    this.radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
    this.toggle = (ToggleButton) findViewById(R.id.toggle);
    this.btnSave = (Button) findViewById(R.id.button_save);
    this.textViewBus = (TextView) findViewById(R.id.textBusId);
    this.textViewSupir = (TextView) findViewById(R.id.textSupirId);
    this.spinnerViewBus = (Spinner) findViewById(R.id.spinnerBusId);
    this.spinnerViewSupir = (Spinner) findViewById(R.id.spinnerSupirID);
  }

  public void getListBus() {
    Call callbus = mAPIService.groupListBus();
    callbus.enqueue(new Callback<List<Bus>>() {
      @Override
      public void onResponse(Call<List<Bus>> call, Response<List<Bus>>
              response) {
        if (response.isSuccessful()) {
          Log.i("JSON Bus List", response.body().toString());
          List<String> listSpinner = new ArrayList<String>();
          for(Bus bus : response.body()) {
            String memberListSpinner = bus.getIdBus() +"."+bus.getPlatNomor() ;
            listSpinner.add(memberListSpinner);
          }
          ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                  android.R.layout.simple_spinner_item, listSpinner);
          adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
          spinnerViewBus.setAdapter(adapter);
        }

      }

      @Override
      public void onFailure(Call call, Throwable t) {
        Log.e("Retrofit Get Bus", t.toString());
      }
    });
  }


  public void getListSupir() {
    Call callsupir = mAPIService.groupListSupir();
    callsupir.enqueue(new Callback<List<Supir>>() {
      @Override
      public void onResponse(Call<List<Supir>> call, Response<List<Supir>>
              response) {
        if (response.isSuccessful()) {
          Log.i("JSON Bus List", response.body().toString());
          List<String> listSpinner = new ArrayList<String>();
          for(Supir supir : response.body()) {
            String memberListSpinner = supir.getIdSupir() +"."+supir.getNamaSupir() ;
            listSpinner.add(memberListSpinner);
          }
          ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                  android.R.layout.simple_spinner_item, listSpinner);
          adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
          spinnerViewSupir.setAdapter(adapter);
        }

      }

      @Override
      public void onFailure(Call call, Throwable t) {
        Log.e("Retrofit Get Supir", t.toString());
      }
    });
  }


  public void openDialog() {

    AlertDialog alertDialog = new AlertDialog.Builder(this).create();

    // Set Custom Title
    TextView title = new TextView(this);
    // Title Properties
    title.setText("Pengaturan Disimpan");
    title.setPadding(10, 10, 10, 10);   // Set Position
    title.setGravity(Gravity.CENTER);
    title.setTextColor(Color.BLACK);
    title.setTextSize(20);
    alertDialog.setCustomTitle(title);

    // Set Button
    // you can more buttons
    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL,"OK", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        // Perform Action on Button
      }
    });


    new Dialog(getApplicationContext());
    alertDialog.show();

    // Set Properties for OK Button
    final Button okBT = alertDialog.getButton(AlertDialog.BUTTON_NEUTRAL);
    LinearLayout.LayoutParams neutralBtnLP = (LinearLayout.LayoutParams) okBT.getLayoutParams();
    neutralBtnLP.gravity = Gravity.FILL_HORIZONTAL;
    okBT.setPadding(50, 10, 10, 10);   // Set Position
    okBT.setTextColor(Color.BLUE);
    okBT.setLayoutParams(neutralBtnLP);


  }
}
