package android.google.com.androidspeedometer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Supir {
    @SerializedName("id")
    @Expose
    private Integer idSupir;
    @SerializedName("nama_supir")
    @Expose
    private String namaSupir;

    public Integer getIdSupir() {
        return idSupir;
    }

    public void setIdSupir(Integer idSupir) {
        this.idSupir = idSupir;
    }

    public String getNamaSupir() {
        return namaSupir;
    }

    public void setNamaSupir(String namaSupir) {
        this.namaSupir = namaSupir;
    }

    @Override
    public String toString() {
        return "Supir{" +
                "idSupir=" + idSupir +
                ", namaSupir='" + namaSupir + '\'' +
                '}';
    }
}
