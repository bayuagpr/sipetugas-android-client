package android.google.com.androidspeedometer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Penumpang {
    @SerializedName("naik_pelajar")
    @Expose
    private Integer naik_pelajar;
    @SerializedName("naik_umum")
    @Expose
    private Integer naik_umum;
    @SerializedName("turun_pelajar")
    @Expose
    private Integer turun_pelajar;
    @SerializedName("turun_umum")
    @Expose
    private Integer turun_umum;

    @SerializedName("jumlah")
    @Expose
    private Integer jumlah;

    @SerializedName("lokasi")
    @Expose
    private String lokasi;

    @SerializedName("bus_id")
    @Expose
    private Integer bus_id;

    @SerializedName("supir_id")
    @Expose
    private Integer supir_id;

    @SerializedName("user_id")
    @Expose
    private Integer user_id;


    public Integer getNaik_pelajar() {
        return naik_pelajar;
    }

    public void setNaik_pelajar(Integer naik_pelajar) {
        this.naik_pelajar = naik_pelajar;
    }

    public Integer getNaik_umum() {
        return naik_umum;
    }

    public void setNaik_umum(Integer naik_umum) {
        this.naik_umum = naik_umum;
    }

    public Integer getTurun_pelajar() {
        return turun_pelajar;
    }

    public void setTurun_pelajar(Integer turun_pelajar) {
        this.turun_pelajar = turun_pelajar;
    }

    public Integer getTurun_umum() {
        return turun_umum;
    }

    public void setTurun_umum(Integer turun_umum) {
        this.turun_umum = turun_umum;
    }

    public Integer getJumlah() {
        return jumlah;
    }

    public void setJumlah(Integer jumlah) {
        this.jumlah = jumlah;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public Integer getBus_id() {
        return bus_id;
    }

    public void setBus_id(Integer bus_id) {
        this.bus_id = bus_id;
    }

    public Integer getSupir_id() {
        return supir_id;
    }

    public void setSupir_id(Integer supir_id) {
        this.supir_id = supir_id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    @Override
    public String toString() {
        return "Penumpang{" +
                "naik_pelajar=" + naik_pelajar +
                ", naik_umum=" + naik_umum +
                ", turun_pelajar=" + turun_pelajar +
                ", turun_umum=" + turun_umum +
                ", jumlah=" + jumlah +
                ", lokasi='" + lokasi + '\'' +
                ", bus_id=" + bus_id +
                ", supir_id=" + supir_id +
                ", user_id=" + user_id +
                '}';
    }
}
