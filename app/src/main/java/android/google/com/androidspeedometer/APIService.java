package android.google.com.androidspeedometer;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface APIService {


    @POST("/api/v1/penumpang")
    @FormUrlEncoded
    Call<Penumpang> savePost(@Field("user_id") int user_id,
                        @Field("supir_id") int supir_id,
                        @Field("bus_id") int bus_id,
                            @Field("jumlah") int jumlah,
                             @Field("lokasi") String lokasi,
                             @Field("turun_umum") int turun_umum ,
                             @Field("turun_pelajar") int turun_pelajar,
                             @Field("naik_umum") int naik_umum ,
                             @Field("naik_pelajar") int naik_pelajar );

    @POST("/api/v1/kecepatan")
    @FormUrlEncoded
    Call<Kecepatan> saveStat(@Field("status") int status,
                             @Field("bus_id") int bus_id,
                             @Field("supir_id") int supir_id);

    @GET("/api/v1/bus")
    Call<List<Bus>> groupListBus();

    @GET("/api/v1/supir")
    Call<List<Supir>> groupListSupir();



}
