package android.google.com.androidspeedometer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Bus {
    @SerializedName("id")
    @Expose
    private Integer idBus;
    @SerializedName("plat_nomer")
    @Expose
    private String platNomor;
    @SerializedName("kapasitas")
    @Expose
    private Integer kapasitas;


    public Integer getIdBus() {
        return idBus;
    }

    public void setIdBus(Integer idBus) {
        this.idBus = idBus;
    }

    public String getPlatNomor() {
        return platNomor;
    }

    public void setPlatNomor(String platNomor) {
        this.platNomor = platNomor;
    }


    public Integer getKapasitas() {
        return kapasitas;
    }

    public void setKapasitas(Integer kapasitas) {
        this.kapasitas = kapasitas;
    }

    @Override
    public String toString() {
        return "Bus{" +
                "idBus=" + idBus +
                ", platNomor='" + platNomor + '\'' +
                ", kapasitas=" + kapasitas +
                '}';
    }
}
